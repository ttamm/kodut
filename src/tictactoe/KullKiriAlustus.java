package tictactoe;

import java.util.Scanner;

public class KullKiriAlustus {

	public static void main(String[] args) {
		KullKiriAlustus();
	}

	public static String KullKiriAlustus() {

		int player1, player2;
		String esimene;

		System.out.println("Player1, vali, kas kull(1) v�i kiri(0) ");
		Scanner scan = new Scanner(System.in);
		player1 = scan.nextInt();

		// kontrollib, et sisestus oleks �ige
		while (player1 != 0 && player1 != 1) {
			System.out.println("Vale sisestus! Vali, kas kull(1) v�i kiri(0).");
			player1 = scan.nextInt();
		}

		// teeb j�reldused
		if (player1 == 1) {
			player2 = 0;
		} else {
			player1 = 0;
			player2 = 1;
		}
		System.out.println("Player1 valis " + player1 + ". Player2 on " + player2);

		int vise = suvalineArv(0, 1);
		System.out.println("M�ndiviske tulemuseks on " + vise);

		if (player1 == vise) {
			esimene = "Player1";
			System.out.println(player1 + " v�itis! Player1 alustab.");
		} else {
			esimene = "Player2";
			System.out.println(player2 + " v�itis! Player2 alustab.");
		}
		return esimene;

	}

	public static int suvalineArv(int min, int max) {

		int vahemik = max - min;

		return min + (int) (Math.random() * (vahemik + 1));

	}

}
