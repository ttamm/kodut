package tictactoe;

import java.util.Scanner;

public class P�hi {

	public static void main(String[] args) {

		char[][] v�li = new char[3][3];

		System.out.println("Lets play Tic-Tac-Toe! \n");

		showBoard(v�li);

		System.out.println();

		// m��rab m�rgi ja alustaja
		// XOMark();

		System.out.println();

		M�nguk�ik(v�li);

		Scanner scan = new Scanner(System.in);
		System.out.println("Would you like to play again?");
		System.out.println("\"1\" kui jah, \"0\" kui ei.");
		int vastus = scan.nextInt();
		while (vastus != 1 && vastus != 0) {
			System.out.println("Vastus peab olema kas \"1\" v�i \"0\"!");
			vastus = scan.nextInt();
		}

		if (vastus == 1) {
			System.out.println("Uus m�ng!");
			M�nguk�ik(v�li);
		} else {
			System.out.println("Thanks for playing!");
			System.exit(0);
		}

	}

	private static void M�nguk�ik(char[][] v�li) {

		int count = 0;
		int i, j;

		System.out.println();
		String esimene = KullKiriAlustus.KullKiriAlustus();
		String teine;

		if (esimene == "Player1") {
			teine = "Player2";
		} else {
			esimene = "Player2";
			teine = "Player1";
		}

		// Esimene k�ik
		System.out.println();
		System.out.println(esimene + "(X), alusta. Pane end valmis, " + teine + "(O)!");
		Scanner scan = new Scanner(System.in);
		System.out.print("Vali tulp(�levalt alla 0-2): ");
		int colValik1 = scan.nextInt();
		while (colValik1 < 0 || colValik1 > 2) {
			System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti!");
			colValik1 = scan.nextInt();
		}
		System.out.print("Vali rida(vasakult paremale 0-2): ");
		int rowValik1 = scan.nextInt();
		count++;
		while (rowValik1 < 0 || rowValik1 > 2) {
			System.out.println("Valik peab olema tehtud 0-2 vahel. Proovi uuesti!");
			rowValik1 = scan.nextInt();
		}
		v�li[colValik1][rowValik1] = 'X';
		for (i = 0; i < v�li.length; i++) {
			for (j = 0; j < v�li.length; j++) {
				System.out.print("__" + v�li[i][j] + "__|");
			}
			System.out.println("");

		}

		System.out.println();

		// Teine k�ik
		if (rowValik1 == 0 || rowValik1 == 1 || rowValik1 == 2) {
			System.out.println(esimene + "(X) tegi oma k�igu �ra. Sinu kord, " + teine + "(O)!");
			System.out.print("Vali tulp(�levalt alla 0-2): ");
			int colValik2 = scan.nextInt();
			while (colValik2 < 0 || colValik2 > 2) {
				System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti!");
				colValik2 = scan.nextInt();
			}
			System.out.print("Vali rida (vasakult paremale 0-2): ");
			int rowValik2 = scan.nextInt();
			count++;
			while (rowValik2 < 0 || rowValik2 > 2) {
				System.out.println("Valik peab olema tehtud 0-2 vahel. Proovi uuesti!");
				rowValik2 = scan.nextInt();
			}
			while (v�li[colValik2][rowValik2] == 'X' || v�li[colValik2][rowValik2] == 'O') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp (�levalt all 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti!");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida (vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti!");
					rowValik2 = scan.nextInt();
				}
			}
			v�li[colValik2][rowValik2] = 'O';

			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");
				}
				System.out.println("");
			}

			System.out.println();

			// Kolmas k�ik
			if (rowValik2 == 0 || rowValik2 == 1 || rowValik2 == 2) {
				System.out.println(teine + "(O) tegi oma k�igu �ra. Sinu kord, " + esimene + "(X)!");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				count++;
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
				while (v�li[colValik1][rowValik1] == 'O' || v�li[colValik1][rowValik1] == 'X') {
					System.out.println("See lahter on juba m�rgitud! Vali teine.");
					System.out.print("Vali tulp (�levalt alla 0-2): ");
					colValik1 = scan.nextInt();
					while (colValik1 < 0 || colValik1 > 2) {
						System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti!");
						colValik1 = scan.nextInt();
					}
					System.out.print("Vali rida (vasakult paremale 0-2): ");
					rowValik1 = scan.nextInt();
					while (rowValik1 < 0 || rowValik1 > 2) {
						System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
						rowValik1 = scan.nextInt();
					}
				}
				v�li[colValik1][rowValik1] = 'X';

				for (i = 0; i < v�li.length; i++) {
					for (j = 0; j < v�li.length; j++) {
						System.out.print("__" + v�li[i][j] + "__|");
					}
					System.out.println("");
				}
			}

			System.out.println();

			// Neljas k�ik
			if (rowValik1 == 0 || rowValik1 == 1 || rowValik1 == 2) {
				System.out.println(esimene + "(X) tegi oma k�igu �ra. Sinu kord, " + teine + "(O)!");
				System.out.print("Vali tulp (�levalt alla 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida (vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				count++;
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			while (v�li[colValik2][rowValik2] == 'X' || v�li[colValik2][rowValik2] == 'O') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp (�levalt alla 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida (vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			v�li[colValik2][rowValik2] = 'O';
			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");

				}
				System.out.println("");
			}

			System.out.println();

			// Viies k�ik
			if (rowValik2 == 0 || rowValik2 == 1 || rowValik2 == 2) {
				System.out.println(teine + "(O) tegi oma k�igu �ra. Sinu kord, " + esimene + "(X)!");
				System.out.print("Vali tulp (�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida (vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				count++;
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
				while (v�li[colValik1][rowValik1] == 'O' || v�li[colValik1][rowValik1] == 'X') {
					System.out.println("See lahter on juba m�rgitud! Vali teine.");
					System.out.print("Vali tulp (�levalt alla 0-2): ");
					colValik1 = scan.nextInt();
					while (colValik1 < 0 || colValik1 > 2) {
						System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
						colValik1 = scan.nextInt();
					}
					System.out.print("Vali rida (vasakult paremale 0-2): ");
					rowValik1 = scan.nextInt();
					while (rowValik1 < 0 || rowValik1 > 2) {
						System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
						rowValik1 = scan.nextInt();
					}
				}
				v�li[colValik1][rowValik1] = 'X';
				for (i = 0; i < v�li.length; i++) {
					for (j = 0; j < v�li.length; j++) {
						System.out.print("__" + v�li[i][j] + "__|");
					}
					System.out.println("");
				}
			}

			System.out.println();

			// Kuues k�ik
			if (rowValik1 == 0 || rowValik1 == 1 || rowValik1 == 2) {
				System.out.println(esimene + "(X) tegi oma k�igu �ra. Sinu kord, " + teine + "(O)!");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				count++;
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			while (v�li[colValik2][rowValik2] == 'X' || v�li[colValik2][rowValik2] == 'O') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.println("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			v�li[colValik2][rowValik2] = 'O';
			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");
				}
				System.out.println("");
			}

			System.out.println();

			// Seitsmes k�ik
			if (rowValik2 == 0 || rowValik2 == 1 || rowValik2 == 2) {
				System.out.println(teine + "(O) tegi oma k�igu �ra. Sinu kord, " + esimene + "(X)!");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				count++;
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
			}
			while (v�li[colValik1][rowValik1] == 'O' || v�li[colValik1][rowValik1] == 'X') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
			}
			v�li[colValik1][rowValik1] = 'X';
			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");
				}
				System.out.println("");
			}

			System.out.println();

			// Kaheksas k�ik
			if (rowValik1 == 0 || rowValik1 == 1 || rowValik1 == 2) {
				System.out.println(esimene + "(X) tegi oma k�igu �ra. Sinu kord, " + teine + "(O)!");
				System.out.print("Vali tulp(�levalt alla 0-2: ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				count++;
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			while (v�li[colValik2][rowValik2] == 'X' || v�li[colValik2][rowValik2] == 'O') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik2 = scan.nextInt();
				while (colValik2 < 0 || colValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik2 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik2 = scan.nextInt();
				while (rowValik2 < 0 || rowValik2 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik2 = scan.nextInt();
				}
			}
			v�li[colValik2][rowValik2] = 'O';
			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");
				}
				System.out.println("");
			}

			System.out.println();

			// �heksas k�ik
			if (rowValik2 == 0 || rowValik2 == 1 || rowValik2 == 2) {
				System.out.println(teine + "(O) tegi oma k�igu �ra. Sinu kord, " + esimene + "(X)!");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				count++;
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
			}
			while (v�li[colValik1][rowValik1] == 'O' || v�li[colValik1][rowValik1] == 'X') {
				System.out.println("See lahter on juba m�rgitud! Vali teine.");
				System.out.print("Vali tulp(�levalt alla 0-2): ");
				colValik1 = scan.nextInt();
				while (colValik1 < 0 || colValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					colValik1 = scan.nextInt();
				}
				System.out.print("Vali rida(vasakult paremale 0-2): ");
				rowValik1 = scan.nextInt();
				while (rowValik1 < 0 || rowValik1 > 2) {
					System.out.print("Valik peab olema 0-2 vahel. Proovi uuesti! ");
					rowValik1 = scan.nextInt();
				}
			}
			v�li[colValik1][rowValik1] = 'X';
			for (i = 0; i < v�li.length; i++) {
				for (j = 0; j < v�li.length; j++) {
					System.out.print("__" + v�li[i][j] + "__|");
				}
				System.out.println("");
			}
		}

		System.out.println();

		V�it(v�li);
		System.out.println("K�ike tehtud: " + count);
		return;

	}

	private static String V�it(char[][] v�li) {

		String esimene = KullKiriAlustus.KullKiriAlustus(); // k�sib uuesti
		String teine;
		String v�itja;

		if (esimene == "Player1") {
			teine = "Player2";
		} else {
			esimene = "Player2";
			teine = "Player1";
		}

		// V�itja m��ramine peale lahtrite t�itumist -
		// http://www.dreamincode.net/forums/topic/325265-tic-tac-toe-wno-gui-in-eclipse/

		// v�it, kui
		if (v�li[0][0] == v�li[0][1] && v�li[0][1] == v�li[0][2] && v�li[0][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[0][0] == v�li[0][1] && v�li[0][1] == v�li[0][2] && v�li[0][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[1][0] == v�li[1][1] && v�li[1][1] == v�li[1][2] && v�li[1][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[1][0] == v�li[1][1] && v�li[1][1] == v�li[1][2] && v�li[1][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[2][0] == v�li[2][1] && v�li[2][1] == v�li[2][2] && v�li[2][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[2][0] == v�li[2][1] && v�li[2][1] == v�li[2][2] && v�li[2][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[0][0] == v�li[1][0] && v�li[1][0] == v�li[2][0] && v�li[1][0] == 'X') {
			System.out.println(esimene + "(X)  v�itis!");
			v�itja = esimene;
		} else if (v�li[0][0] == v�li[1][0] && v�li[1][0] == v�li[2][0] && v�li[1][0] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[0][1] == v�li[1][1] && v�li[1][1] == v�li[2][1] && v�li[1][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[0][1] == v�li[1][1] && v�li[1][1] == v�li[2][1] && v�li[1][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[0][2] == v�li[1][2] && v�li[1][2] == v�li[2][2] && v�li[1][2] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[0][2] == v�li[1][2] && v�li[1][2] == v�li[2][2] && v�li[1][2] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[0][0] == v�li[1][1] && v�li[1][1] == v�li[2][2] && v�li[1][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[0][0] == v�li[1][1] && v�li[1][1] == v�li[2][2] && v�li[1][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else if (v�li[2][0] == v�li[1][1] && v�li[1][1] == v�li[0][2] && v�li[1][1] == 'X') {
			System.out.println(esimene + "(X) v�itis!");
			v�itja = esimene;
		} else if (v�li[2][0] == v�li[1][1] && v�li[1][1] == v�li[0][2] && v�li[1][1] == 'O') {
			System.out.println(teine + "(O) v�itis!");
			v�itja = teine;
		} else {
			System.out.println("V�itja puudub! Viik!");
			v�itja = " ";
		} System.out.println();

		return v�itja;
	}

	// Choosing the mark for players, hetkel pole kasutuses
	private static char XOMark() {
		char player1, player2;

		System.out.print("Player1, palun vali m�rk(X v�i O): ");
		Scanner scan = new Scanner(System.in);
		player1 = scan.next().charAt(0);

		// kontrollib sisestuse �igsust
		while ((player1 != 'X' && player1 != 'x') && (player1 != 'O' && player1 != 'o')) {
			System.out.print("Valida saab ainult kahe vahel - kas X v�i O. Proovi uuesti! ");
			player1 = scan.next().charAt(0);
		}

		if (player1 == 'O' || player1 == 'o') {
			player1 = 'O';
			player2 = 'X';
			System.out.println("Player1 valis " + player1 + ". Player2 on " + player2);
			return player2;

		} else if (player1 == 'X' || player1 == 'x') {
			player1 = 'X';
			player2 = 'O';
			System.out.println("Player1 valis " + player1 + ". Player2 on " + player2);

			return player2;
		}
		return player1;

	}

	public static void showBoard(char[][] v�li) {

		int i, j;

		v�li[0][0] = ' ';
		v�li[0][1] = ' ';
		v�li[0][2] = ' ';
		v�li[1][0] = ' ';
		v�li[1][1] = ' ';
		v�li[1][2] = ' ';
		v�li[2][0] = ' ';
		v�li[2][1] = ' ';
		v�li[2][2] = ' ';

		for (i = 0; i < v�li.length; i++) {
			for (j = 0; j < v�li.length; j++) {
				System.out.print("__" + v�li[i][j] + "__|");
			}
			System.out.println("");
		}

	}
}